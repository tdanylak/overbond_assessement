### To run the app

`ruby overbond.rb [input_file] [output_file]`


## To Test the app

#### first run this command if minitest is not installed

`bundle install`  


#### To run the test

`ruby overbond_test.rb`


*** Docker. I have not setup a docker image in a while, and currently don't have the Docker setup on my machine to test it out properly ***
