class BondBenchmark

  def self.benchmark_bonds(corporate_bond, government_bonds)

    corp_tenor = corporate_bond["tenor"].to_f
    tenor_spread = 0
    best_benchmark = nil

    government_bonds.each do |b|
      gov_tenor = b["tenor"].to_f

      tenor_spread = (corp_tenor - gov_tenor).abs if tenor_spread == 0

      unless ((corp_tenor - gov_tenor).abs) > tenor_spread
        best_benchmark = b
        tenor_spread = (corp_tenor - gov_tenor).abs
      end

    end

    return best_benchmark
  end

end
