class BondValidator
  def BondValidator.validate(bond)
    return false if bond == nil

    bond.keys.sort.each do |key|
      return false if bond[key] == nil
    end
    
    return bond
  end
end
