require 'minitest/autorun'

require 'json'
require "./bond_validator.rb"
require "./bond_benchmark.rb"
require "./bond_matcher.rb"

class OverbondTest < Minitest::Test


  def test_bond_validator
    valid_bond = {
  			"id": "c1",
  			"type": "corporate",
  			"tenor": "10.3 years",
  			"yield": "5.30%",
  			"amount_outstanding": 1200000
  		}

    invalid_bond = {
  			"id": "c3",
  			"type": "corporate",
  			"tenor": "10.3 years",
  			"yield": nil,
  			"amount_outstanding": 1200000
  		}

    assert BondValidator.validate(valid_bond), "This is a valid bond"
    assert ! BondValidator.validate(invalid_bond), "This is an invalid bond"
  end

  def test_bond_benchmark
    c_bond = {
        "id": "c1",
        "type": "corporate",
        "tenor": "10.3 years",
        "yield": "5.30%",
        "amount_outstanding": 1200000
      }

    g_bonds =  [{
        "id": "g1",
        "type": "government",
        "tenor": "5.3 years",
        "yield": "11.0%",
        "amount_outstanding": 1200000
      }, {
          "id": "g2",
          "type": "government",
          "tenor": "11.3 years",
          "yield": "12.0%",
          "amount_outstanding": 1200000
        },
        {
            "id": "g2",
            "type": "government",
            "tenor": "12.3 years",
            "yield": "13.0%",
            "amount_outstanding": 1200000
          }
      ]

    benchmark = BondBenchmark.benchmark_bonds(c_bond, g_bonds)

    assert benchmark[:id] == "g2", "G2 Is the closest benchmark"

  end

  def test_bond_matcher
    input_file = "test_input.json"
    output_file = "test_output.json"

    matcher = BondMatcher.new input_file, output_file
    matcher.match_and_write_to_output

    ## read the file and make sure it's good
    matched_bonds = JSON.parse(File.read(output_file))

    assert matched_bonds["data"].size, 1

    bond = matched_bonds["data"].first


    ## If the data in in test_input.json changes, this will have to be updated
    assert bond["corporate_bond_id"] == "c1", "Match first corp bond"
    assert bond["government_bond_id"] == "g1", "Match first gov bond"
    assert bond["spread_to_benchmark"] == "160 bps", "Spread"

  end

end
