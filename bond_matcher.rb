class BondMatcher

  def initialize(input_file, output_file)
    @input_filename = input_file
    @output_filename = output_file
  end

  def match_and_write_to_output
    ## Parse JSON
    unsorted_bonds = JSON.parse(File.read(@input_filename))

    ## Setup arrays of sorted and paried bonds
    corporate_bonds = []
    government_bonds = []
    paired_bonds = []

    # sort bonds
    unsorted_bonds["data"].each do |b|

      unless BondValidator.validate(b) == false
        corporate_bonds.push(b) if b["type"] == "corporate"
        government_bonds.push(b) if b["type"] == "government"
      end
    end


    ## benchmark bonds
    corporate_bonds.each do |b|
      best_benchmark = BondBenchmark.benchmark_bonds(b, government_bonds)

      if best_benchmark != nil
        c_yield = b["yield"].scan(/\d/).join('')
        g_yield = best_benchmark["yield"].scan(/\d/).join('')

        spread = c_yield.to_i - g_yield.to_i

        paired_bonds.push ({
              "corporate_bond_id": b["id"],
              "government_bond_id": best_benchmark["id"],
              "spread_to_benchmark": "#{spread} bps"
            })
      end
    end


    ## Process Output
    File.write(@output_filename, JSON.dump({ "data": paired_bonds} ) )
  end
end
