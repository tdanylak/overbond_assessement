require 'json'
require "./bond_validator.rb"
require "./bond_benchmark.rb"
require "./bond_matcher.rb"


input_filename = ARGV[0]
output_filename = ARGV[1]


puts "Processing #{input_filename} into #{output_filename}"

matcher = BondMatcher.new input_filename, output_filename
matcher.match_and_write_to_output
